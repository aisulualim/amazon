package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AmazonSearchPage;
import pages.BasePage;
import pages.ProductPage;

public class AmazonTest extends BasePage {

    public AmazonSearchPage amazonSearchPage;
    public ProductPage productPage;

    @Test
    public void test1() throws InterruptedException {
        productPage = new ProductPage(driver);
        amazonSearchPage = new AmazonSearchPage(driver);
        driver.navigate().to("https://www.amazon.com/");
        amazonSearchPage.enterKeyWordInToSearchBar("Alexa");
        Thread.sleep(2000); //better to use explicit wait but didn't have time to implement
        amazonSearchPage.secondPage.click();
        Thread.sleep(2000);
        amazonSearchPage.listOfProducts.get(3).click();
        Thread.sleep(2000);
        productPage.addToCard.click();
        Thread.sleep(2000);
        productPage.noThanksButton.click();
        Thread.sleep(2000);
        Assert.assertEquals(productPage.addToCartConfirmation.getText(), "Added to Cart");
    }
}

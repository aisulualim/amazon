package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage extends BasePage {

    public ProductPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "add-to-cart-button")
    public WebElement addToCard;

    @FindBy(id = "siNoCoverage-announce")
    public WebElement noThanksButton;

    @FindBy(xpath = "//h1[@class='a-size-medium a-text-bold']")
    public WebElement addToCartConfirmation;



}

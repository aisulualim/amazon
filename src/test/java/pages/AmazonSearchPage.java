package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class AmazonSearchPage extends BasePage {

    public AmazonSearchPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "twotabsearchtextbox")
    public WebElement searchBar;

    @FindBy(id = "nav-search-submit-text")
    public WebElement searchButton;

    @FindBy(linkText = "2")
    public WebElement secondPage;

    @FindBy(xpath = "//span[@class='a-size-medium a-color-base a-text-normal']")
    public List<WebElement> listOfProducts;

    public void enterKeyWordInToSearchBar(String name) {
        searchBar.click();
        searchBar.sendKeys(name);
        searchBar.submit();
    }

    }


package pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BasePage {

    public WebDriver driver;

    @BeforeClass
    public void loadPropAndDriver() {

        Properties properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String browser = properties.getProperty("browser");
        switch (Integer.valueOf(browser)) {
            case 1:
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;
            case 2:
                    WebDriverManager.chromedriver().setup();
                    driver = new SafariDriver();
                    break;
        }
    }

    @AfterClass
    public void tearsDown(){
        driver.quit();
    }

    public boolean isElementDisplayed(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public boolean clickElement(WebElement element) {
        isElementDisplayed(element);
        element.click();
        return true;
    }


}
